package com.desafio.springboot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desafio.springboot.check.ProductsCheck;
import com.desafio.springboot.model.Product;
import com.desafio.springboot.model.ProductRepository;

@Service
public class NewProductService {
	
	@Autowired
	private ProductsCheck productsCheck;
	
	@Autowired
	private ProductRepository productsRepository;
	
	public Product process(Product products) throws Exception {
		
		productsCheck.check(products);
		
		final Product newProducts = productsRepository.save(products);
		return newProducts;
	}
}
