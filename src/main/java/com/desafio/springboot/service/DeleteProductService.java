package com.desafio.springboot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.desafio.springboot.model.ProductRepository;

@Service
public class DeleteProductService {
	
	@Autowired
	private ProductRepository productsRepository;

	public HttpStatus process(String idProduto) {
		if (!productsRepository.existsById(idProduto)) {
			return HttpStatus.NOT_FOUND;
		}
		
		productsRepository.deleteById(idProduto);
		return HttpStatus.OK;
	}
}
