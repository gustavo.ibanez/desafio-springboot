FROM openjdk:8
EXPOSE 9999
ADD target/desafio-springboot.jar desafio-springboot.jar
ENTRYPOINT ["java", "-jar", "desafio-springboot.jar"]