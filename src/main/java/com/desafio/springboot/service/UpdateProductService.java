package com.desafio.springboot.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desafio.springboot.check.ProductsCheck;
import com.desafio.springboot.model.Product;
import com.desafio.springboot.model.ProductRepository;
import com.desafio.springboot.model.ProductUpdateDTO;

@Service
public class UpdateProductService {
	
	@Autowired
	private ProductsCheck productsCheck;
	
	@Autowired
	private ProductRepository productsRepository;

	public Product process(String idProduct, ProductUpdateDTO productDTO) throws Exception {
		
		final Optional<Product> optionalProduct = productsRepository.findById(idProduct);
		if (!optionalProduct.isPresent()) {
			return null;
		}
		
		final Product updateProduct = optionalProduct.get();
		updateProduct.setName(productDTO.getName());
		updateProduct.setDescription(productDTO.getDescription());
		updateProduct.setPrice(productDTO.getPrice());
		
		productsCheck.check(updateProduct);
		
		return productsRepository.save(updateProduct);
	}
}
