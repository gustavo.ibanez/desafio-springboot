package com.desafio.springboot.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseErrorDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("status_code")
	private Integer statusCode;
	
	@JsonProperty("message")
	private String message;
	
	public ResponseErrorDTO(Integer erroCode, String errodMessage) {
		this.message = errodMessage;
		this.statusCode = erroCode;
	}
	
	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
