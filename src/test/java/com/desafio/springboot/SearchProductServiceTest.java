package com.desafio.springboot;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasProperty;

import java.util.List;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import com.desafio.springboot.build.ProductsBuilder;
import com.desafio.springboot.model.Product;
import com.desafio.springboot.service.SearchProductService;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class SearchProductServiceTest {

	@Autowired 
	private SearchProductService searchProductService;
	
	@Autowired
	private ProductsBuilder productsBuilder;
	
	@Before
	public void setup() {
		productsBuilder.build();
	}
	
	@Test
	public void givenAllParametersWhenSearchServiceRunExpectedReturnItem() {
		final List<Product> products = searchProductService.process("Procuct", 1d, 100d);
		MatcherAssert.assertThat(products, Matchers.hasSize(3));
	}
	
	@Test
	public void givenNameParametersWhenSearchServiceRunExpectedReturnItem() {
		final List<Product> products = searchProductService.process("Procuct 1", null, null);
		assertThat(products, Matchers.hasSize(1));
		assertThat(products, Matchers.<Product>hasItem(
					allOf(
						hasProperty("name", Matchers.equalTo("Test Procuct 1")),
						hasProperty("description", Matchers.equalTo("Product Description 1")),
						hasProperty("price", Matchers.equalTo(21.03d))
					)
				));
	}
	
	@Test
	public void givenDescriptionParametersWhenSearchServiceRunExpectedReturnItem() {
		final List<Product> products = searchProductService.process("new version", null, null);
		assertThat(products, Matchers.hasSize(1));
		assertThat(products, Matchers.<Product>hasItem(
					allOf(
						hasProperty("name", Matchers.equalTo("Test Procuct 3")),
						hasProperty("description", Matchers.equalTo("Product Description 1.1 - new version")),
						hasProperty("price", Matchers.equalTo(32.01d))
					)
				));
	}
	
	@Test
	public void givenValueMinParametersWhenSearchServiceRunExpectedReturnItem() {
		final List<Product> products = searchProductService.process(null, 30d, null);
		assertThat(products, Matchers.hasSize(2));
		assertThat(products, Matchers.<Product>hasItems(
					allOf(
						hasProperty("name", Matchers.equalTo("Test Procuct 2")),
						hasProperty("description", Matchers.equalTo("Product Description 2")),
						hasProperty("price", Matchers.equalTo(57.03d))
					),
					allOf(
						hasProperty("name", Matchers.equalTo("Test Procuct 3")),
						hasProperty("description", Matchers.equalTo("Product Description 1.1 - new version")),
						hasProperty("price", Matchers.equalTo(32.01d))
					)
				));
	}
	
	@Test
	public void givenValueMaxParametersWhenSearchServiceRunExpectedReturnItem() {
		final List<Product> products = searchProductService.process(null, null, 40d);
		assertThat(products, Matchers.hasSize(2));
		assertThat(products, Matchers.<Product>hasItems(
					allOf(
						hasProperty("name", Matchers.equalTo("Test Procuct 3")),
						hasProperty("description", Matchers.equalTo("Product Description 1.1 - new version")),
						hasProperty("price", Matchers.equalTo(32.01d))
					),
					allOf(
						hasProperty("name", Matchers.equalTo("Test Procuct 1")),
						hasProperty("description", Matchers.equalTo("Product Description 1")),
						hasProperty("price", Matchers.equalTo(21.03d))
					)
				));
	}
	
	@Test
	public void givenWithoutParametersWhenSearchServiceRunExpectedReturnItem() {
		final List<Product> products = searchProductService.process(null, null, null);
		assertThat(products, Matchers.hasSize(3));
	}
	
}
