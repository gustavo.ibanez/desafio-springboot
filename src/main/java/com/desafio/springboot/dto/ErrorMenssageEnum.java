package com.desafio.springboot.dto;

public enum ErrorMenssageEnum {
	
	INVALID_PRODUCT_NAME("Field 'Name' must have a value"),
	INVALID_PRODUCT_DESCRIPTION("Field 'Description' must have a value"),
	INVALID_PRODUCT_PRICE("Field 'Price' must have a value"),
	INVALID_VALUE_PRODUCT_PRICE("Field 'Price' must be greater than zero"),
	PRODUCT_NOT_FOUND("Product not found")
	;
	
	private final String message;
	
	private ErrorMenssageEnum(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
