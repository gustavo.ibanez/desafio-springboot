package com.desafio.springboot;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import com.desafio.springboot.build.ProductsBuilder;
import com.desafio.springboot.model.Product;
import com.desafio.springboot.model.ProductRepository;
import com.desafio.springboot.model.ProductUpdateDTO;
import com.desafio.springboot.service.UpdateProductService;
import com.desafio.springboot.util.InvalidProductNameException;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class UpdateProductServiceTest {

	@Autowired 
	private UpdateProductService updateProductService;
	
	@Autowired 
	private ProductRepository productRepository;
	
	@Autowired
	private ProductsBuilder productsBuilder;
	
	@Before
	public void setup() {
		productsBuilder.build();
	}
	
	@Test
	public void givenProductToUpdateWhenUpdateServiceRunExpectedProductUpdated() throws Exception {
        final ProductUpdateDTO productDto = new ProductUpdateDTO.ProductUpdateDtoBuilder()
        	.setName("Test Procuct 1 - Updated")
        	.setDescription("Product Description 1 - Updated")
        	.setPrice(26.10d)
        	.build();
        
		final Product productsUpdated = updateProductService.process("1", productDto);
		assertThat(productsUpdated, Matchers.notNullValue());
		assertThat(productsUpdated.getId(), Matchers.equalTo("1"));
		
		
		final Optional<Product> optionalProductUpdated = productRepository.findById("1");
        assertTrue(optionalProductUpdated.isPresent());
        final Product product1 = optionalProductUpdated.get();
        assertThat(product1.getId(), Matchers.equalTo("1"));
        assertThat(product1.getName(), Matchers.equalTo("Test Procuct 1 - Updated"));
        assertThat(product1.getDescription(), Matchers.equalTo("Product Description 1 - Updated"));
        assertThat(product1.getPrice(), Matchers.equalTo(26.10d));
	}	
	
	@Test(expected=InvalidProductNameException.class)
	public void givenInvalidProductWhenUpdateProductServiceRunExpectedErro() throws Exception {
		final ProductUpdateDTO productDto = new ProductUpdateDTO.ProductUpdateDtoBuilder()
					.setName(null)
                	.setDescription("Product Description 1")
                	.setPrice(21.03d)
                	.build();
        
        updateProductService.process("1", productDto);
	}
	
	public void givenInvalidIdWhenUpdateProductServiceRunExpectedErro() throws Exception {
		final ProductUpdateDTO productDto = new ProductUpdateDTO.ProductUpdateDtoBuilder()
			.setName("Product 1")
			.setDescription("Product Description 1")
			.setPrice(21.03d)
			.build();
		
		final Product productsUpdated = updateProductService.process("20", productDto);
		assertThat(productsUpdated, Matchers.nullValue());
	}
}
