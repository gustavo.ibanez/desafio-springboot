package com.desafio.springboot.util;

import com.desafio.springboot.dto.ErrorMenssageEnum;

public class InvalidProductPriceException extends Exception{

	private static final long serialVersionUID = 1L;

	public InvalidProductPriceException() {
		super(ErrorMenssageEnum.INVALID_PRODUCT_PRICE.getMessage());
	}
}
