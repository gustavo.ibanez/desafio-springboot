package com.desafio.springboot;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import com.desafio.springboot.build.ProductsBuilder;
import com.desafio.springboot.model.Product;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class ProductsControllerTest {
	
	@LocalServerPort
    private int port;
	
	@Autowired
	private ProductsBuilder productsBuilder;
	
	@Before
	public void setup() {
		productsBuilder.build();
	}
	
	@Test
    public void giveProductsWhenCallAPIGetAllExpectedJsonWithAllProduct() throws JSONException {
		final ResponseEntity<String> response = CallAPIProductController( "/api/products", HttpMethod.GET, null);

        final String expectedValue = "[{\"id\":\"1\",\"name\":\"Test Procuct 1\",\"description\":\"Product Description 1\",\"price\":21.03},{\"id\":\"2\",\"name\":\"Test Procuct 2\",\"description\":\"Product Description 2\",\"price\":57.03},{\"id\":\"3\",\"name\":\"Test Procuct 3\",\"description\":\"Product Description 1.1 - new version\",\"price\":32.01}]";
		JSONAssert.assertEquals(expectedValue, response.getBody(), false);
		MatcherAssert.assertThat(response.getStatusCode(), Matchers.equalTo(HttpStatus.OK));
	}

	@Test
	public void giveProductsWhenCallAPIGetSpecificProductExpectedJsonWithOneProduct() throws JSONException {
		final ResponseEntity<String> response = CallAPIProductController( "/api/products/1", HttpMethod.GET, null);
		final String expectedValue = "{\"id\":\"1\",\"name\":\"Test Procuct 1\",\"description\":\"Product Description 1\",\"price\":21.03}";
		JSONAssert.assertEquals(expectedValue, response.getBody(), false);
		MatcherAssert.assertThat(response.getStatusCode(), Matchers.equalTo(HttpStatus.OK));
	}
	
	@Test
	public void giveProductsWhenCallAPIGetWithInvalidIdExpectedError() throws JSONException {
		final ResponseEntity<String> response = CallAPIProductController( "/api/products/100", HttpMethod.GET, null);
		
		MatcherAssert.assertThat(response.getStatusCode(), Matchers.equalTo(HttpStatus.NOT_FOUND));
		MatcherAssert.assertThat(response.getBody(), Matchers.nullValue());
	}	
	
	@Test
	public void giveNewProductWhenCallAPIPostExpectedNewProduct() throws JSONException {
		final Product newProduct = new Product();
        newProduct.setName("Test Procuct 4");
        newProduct.setDescription("Product Description 4");
        newProduct.setPrice(40.03d);		
		final ResponseEntity<String> response = CallAPIProductController( "/api/products", HttpMethod.POST, newProduct);
		
		final String expectedValue = "{\"id\":\"4\",\"name\":\"Test Procuct 4\",\"description\":\"Product Description 4\",\"price\":40.03}";
		JSONAssert.assertEquals(expectedValue, response.getBody(), false);
		MatcherAssert.assertThat(response.getStatusCode(), Matchers.equalTo(HttpStatus.CREATED));
	}
	
	@Test
	public void giveInvalidProductWhenCallAPIPostExpectedError() throws JSONException {
		final Product newProduct = new Product();
		newProduct.setName(null);
		newProduct.setDescription("Product Description 4");
		newProduct.setPrice(40.03d);		
		final ResponseEntity<String> response = CallAPIProductController( "/api/products", HttpMethod.POST, newProduct);
		MatcherAssert.assertThat(response.getStatusCode(), Matchers.equalTo(HttpStatus.BAD_REQUEST));
	}
	
	@Test
	public void giveProductWhenCallAPIPutExpectedUpdatedProduct() throws JSONException {
		final Product updateProduct = new Product();
        updateProduct.setName("Test Procuct 2 - Updated");
        updateProduct.setDescription("Product Description 2 - Updated");
        updateProduct.setPrice(33.03d);
		final ResponseEntity<String> response = CallAPIProductController( "/api/products/2", HttpMethod.PUT, updateProduct);
		
		final String expectedValue = "{\"id\":\"2\",\"name\":\"Test Procuct 2 - Updated\",\"description\":\"Product Description 2 - Updated\",\"price\":33.03}";
		JSONAssert.assertEquals(expectedValue, response.getBody(), false);
		MatcherAssert.assertThat(response.getStatusCode(), Matchers.equalTo(HttpStatus.OK));
	}
	
	@Test
	public void giveInvalidProductWhenCallAPIPutExpectedUpdatedProduct() throws JSONException {
		final Product updateProduct = new Product();
		updateProduct.setName(null);
		updateProduct.setDescription("Product Description 2 - Updated");
		updateProduct.setPrice(33.03d);
		
		final ResponseEntity<String> response = CallAPIProductController( "/api/products/2", HttpMethod.PUT, updateProduct);
		MatcherAssert.assertThat(response.getStatusCode(), Matchers.equalTo(HttpStatus.BAD_REQUEST));
	}
	
	@Test
	public void giveProductWhenCallAPIPutWithInvalidIdExpectedError() throws JSONException {
		final Product updateProduct = new Product();
		updateProduct.setName(null);
		updateProduct.setDescription("Product Description 2 - Updated");
		updateProduct.setPrice(33.03d);
		
		final ResponseEntity<String> response = CallAPIProductController( "/api/products/200", HttpMethod.PUT, updateProduct);
		MatcherAssert.assertThat(response.getStatusCode(), Matchers.equalTo(HttpStatus.NOT_FOUND));
	}
	
	@Test
	public void giveProductsWhenCallAPIDeleteExpectedDeletedProduct() throws JSONException {
		final ResponseEntity<String> response = CallAPIProductController( "/api/products/2", HttpMethod.DELETE, null);
		MatcherAssert.assertThat(response.getStatusCode(), Matchers.equalTo(HttpStatus.OK));
	}
	
	@Test
	public void giveProductsWhenCallAPIDeleteWithInvalidIdExpectedError() throws JSONException {
		final ResponseEntity<String> response = CallAPIProductController( "/api/products/200", HttpMethod.DELETE, null);
		MatcherAssert.assertThat(response.getStatusCode(), Matchers.equalTo(HttpStatus.NOT_FOUND));
	}
	
	@Test
	public void giveProductsWhenCallAPIGetSearchExpectedProduct() throws JSONException {
		final ResponseEntity<String> response = CallAPIProductController( "/api/products/search?min_price=10.55&max_price=30.54&name_description=Product", HttpMethod.GET, null);
		
		final String expectedValue = "[{\"id\":\"1\",\"name\":\"Test Procuct 1\",\"description\":\"Product Description 1\",\"price\":21.03}]";
		JSONAssert.assertEquals(expectedValue, response.getBody(), false);
		MatcherAssert.assertThat(response.getStatusCode(), Matchers.equalTo(HttpStatus.OK));
	}
	
	@Test
	public void giveProductsWhenCallAPIGetSearchWithNotFoudParamExpectedEmptyJson() throws JSONException {
		final ResponseEntity<String> response = CallAPIProductController( "/api/products/search?name_description=notFound", HttpMethod.GET, null);
		
		final String expectedValue = "[]";
		JSONAssert.assertEquals(expectedValue, response.getBody(), false);
		MatcherAssert.assertThat(response.getStatusCode(), Matchers.equalTo(HttpStatus.OK));
	}
	
	private ResponseEntity<String> CallAPIProductController(String url, HttpMethod httpMethod, Product request) {
		final HttpHeaders headers = new HttpHeaders();
		final TestRestTemplate restTemplate = new TestRestTemplate();
		final HttpEntity<Product> entity = new HttpEntity<Product>(request, headers);
        final ResponseEntity<String> response = restTemplate.exchange(getUrlBase() + url, httpMethod, entity, String.class);
		return response;
	}
	
	private String getUrlBase() {
		return "http://localhost:" + port;
	}
}
