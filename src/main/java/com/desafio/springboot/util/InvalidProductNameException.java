package com.desafio.springboot.util;

import com.desafio.springboot.dto.ErrorMenssageEnum;

public class InvalidProductNameException extends Exception{

	private static final long serialVersionUID = 1L;

	public InvalidProductNameException() {
		super(ErrorMenssageEnum.INVALID_PRODUCT_NAME.getMessage());
	}
}
