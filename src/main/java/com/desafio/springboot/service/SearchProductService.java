package com.desafio.springboot.service;

import static org.apache.commons.lang3.StringUtils.contains;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desafio.springboot.model.Product;
import com.desafio.springboot.model.ProductRepository;

@Service
public class SearchProductService {
	
	@Autowired
	private ProductRepository productsRepository;

	public Optional<Product> findById(String idProduto) {
		final Optional<Product> product = productsRepository.findById(idProduto);
		return product;
	}
	
	public List<Product> findAllProducts( ) {
		return productsRepository.findAll();
	}  
	
	public List<Product> process(String valueQ, Double valueMin, Double valueMax) {
		
		final List<Product> procucts = productsRepository.findAll();
		
		final List<Product> result = procucts.stream()
			.filter(predicateFilterValueQ(valueQ))
			.filter(predicateFilterValueMin(valueMin))
			.filter(predicateFilterValueMax(valueMax))
			.collect(Collectors.toList());
		
		return result;
		
	}

	private Predicate<Product> predicateFilterValueQ(String valueQ) {
		return new Predicate<Product>() {

			@Override
			public boolean test(Product t) {
				if (StringUtils.isNotBlank(valueQ)) {
					return contains(t.getDescription().toUpperCase(), valueQ.toUpperCase()) || contains(t.getName().toUpperCase(), valueQ.toUpperCase()); 
				}
				return true;
			}
		};
	}
	
	private Predicate<Product> predicateFilterValueMin(Double valueMin) {
		return new Predicate<Product>() {
			
			@Override
			public boolean test(Product t) {
				if (valueMin != null) {
					return t.getPrice() >= valueMin;
				}
				return true;
			}
		};
	}
	
	private Predicate<Product> predicateFilterValueMax(Double valueMax) {
		return new Predicate<Product>() {
			
			@Override
			public boolean test(Product t) {
				if (valueMax != null) {
					return t.getPrice() <= valueMax;
				}
				return true;
			}
		};
	}
}
