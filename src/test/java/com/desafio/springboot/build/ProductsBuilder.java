package com.desafio.springboot.build;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.desafio.springboot.model.Product;
import com.desafio.springboot.model.ProductRepository;

@Component
public class ProductsBuilder {

	@Autowired
	private ProductRepository productRepository;
	
	public void build() {
		final Product product1 = new Product();
        product1.setName("Test Procuct 1");
        product1.setDescription("Product Description 1");
        product1.setPrice(21.03d);
		
        productRepository.save(product1);
        
        final Product product2 = new Product();
        product2.setName("Test Procuct 2");
        product2.setDescription("Product Description 2");
        product2.setPrice(57.03d);
		
        productRepository.save(product2);
        
        final Product product3 = new Product();
        product3.setName("Test Procuct 3");
        product3.setDescription("Product Description 1.1 - new version");
        product3.setPrice(32.01d);
		
        productRepository.save(product3);
	}
	
}
