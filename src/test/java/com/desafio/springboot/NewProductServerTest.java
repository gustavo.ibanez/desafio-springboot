package com.desafio.springboot;

import static org.hamcrest.MatcherAssert.assertThat;

import java.util.List;
import java.util.Optional;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import com.desafio.springboot.model.Product;
import com.desafio.springboot.model.ProductRepository;
import com.desafio.springboot.service.NewProductService;
import com.desafio.springboot.util.InvalidProductNameException;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class NewProductServerTest {

	@Autowired 
	private NewProductService newProductService;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Test
	public void givenProductWhenNewProductServiceRunExpectedItemInserted() throws Exception {
		final List<Product> allProducts = productRepository.findAll();
		assertThat(allProducts, Matchers.hasSize(0));
		
		final Product product = new Product();
        product.setName("Test Procuct 1");
        product.setDescription("Product Description 1");
        product.setPrice(21.03d);
        
        final Product newProduct = newProductService.process(product);
        MatcherAssert.assertThat(newProduct.getId(), Matchers.notNullValue());
        
        final Optional<Product> optionalProduct = productRepository.findById(newProduct.getId());
        Assert.assertTrue(optionalProduct.isPresent());
        
		assertThat(productRepository.findAll(), Matchers.hasSize(1));
	}
	
	@Test(expected=InvalidProductNameException.class)
	public void givenInvalidProductWhenNewProductServiceRunExpectedErro() throws Exception {
		final Product product = new Product();
        product.setName(null);
        product.setDescription("Product Description 1");
        product.setPrice(21.03d);
        
        newProductService.process(product);
	}
}
