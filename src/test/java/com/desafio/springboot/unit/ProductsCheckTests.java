package com.desafio.springboot.unit;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.mockito.MockitoAnnotations.openMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;

import com.desafio.springboot.check.ProductsCheck;
import com.desafio.springboot.model.Product;
import com.desafio.springboot.util.InvalidProductDescriptionException;
import com.desafio.springboot.util.InvalidProductNameException;
import com.desafio.springboot.util.InvalidProductPriceException;
import com.desafio.springboot.util.InvalidValueProductPriceException;

public class ProductsCheckTests {

	@InjectMocks
	private ProductsCheck productsCheck;
	
	private Product product;
	
	@Before
    public void setUp() {
        openMocks(this);
        
        product = new Product();
        product.setName("Test Procuct");
        product.setDescription("Product Description");
        product.setPrice(21.03d);
    }
	
	@Test
	public void givenCorrectProductWhenRunProductsCheckDontExpectedError() throws Exception {
		productsCheck.check(product);
	}
	
	@Test(expected=InvalidProductNameException.class)
	public void givenProductWithNullNameWhenRunProcuctsCheckExpectedError() throws Exception {
		product.setName(null);
		productsCheck.check(product);
	}
	
	@Test(expected=InvalidProductNameException.class)
	public void givenProductWithEmptyNameWhenRunProcuctsCheckExpectedError() throws Exception {
		product.setName(EMPTY);
		productsCheck.check(product);
	}
	
	@Test(expected=InvalidProductNameException.class)
	public void givenProductWithInvalidNameWhenRunProcuctsCheckExpectedError() throws Exception {
		product.setName(" ");
		productsCheck.check(product);
	}
	
	@Test(expected=InvalidProductDescriptionException.class)
	public void givenProductWithNullDescriptionWhenRunProcuctsCheckExpectedError() throws Exception {
		product.setDescription(null);
		productsCheck.check(product);
	}
	
	@Test(expected=InvalidProductDescriptionException.class)
	public void givenProductWithEmptyDescriptionWhenRunProcuctsCheckExpectedError() throws Exception {
		product.setDescription(EMPTY);
		productsCheck.check(product);
	}	
	
	@Test(expected=InvalidProductDescriptionException.class)
	public void givenProductWithInvalidDescriptionWhenRunProcuctsCheckExpectedError() throws Exception {
		product.setDescription(" ");
		productsCheck.check(product);
	}
	
	@Test(expected=InvalidProductPriceException.class)
	public void givenProductWithOutPriceWhenRunProcuctsCheckExpectedError() throws Exception {
		product.setPrice(null);
		productsCheck.check(product);
	}
	
	@Test(expected=InvalidValueProductPriceException.class)
	public void givenProductWithPriceEqualZeroWhenRunProcuctsCheckExpectedError() throws Exception {
		product.setPrice(0d);
		productsCheck.check(product);
	}
	
	@Test(expected=InvalidValueProductPriceException.class)
	public void givenProductWithPriceLessZeroWhenRunProcuctsCheckExpectedError() throws Exception {
		product.setPrice(-10d);
		productsCheck.check(product);
	}
}

