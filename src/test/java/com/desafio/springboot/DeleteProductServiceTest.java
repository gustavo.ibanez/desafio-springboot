package com.desafio.springboot;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import com.desafio.springboot.build.ProductsBuilder;
import com.desafio.springboot.model.Product;
import com.desafio.springboot.model.ProductRepository;
import com.desafio.springboot.service.DeleteProductService;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class DeleteProductServiceTest {

	@Autowired 
	private DeleteProductService deleteProductService;
	
	@Autowired
	private ProductsBuilder productsBuilder;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Before
	public void setup() {
		productsBuilder.build();
	}
	
	@Test
	public void givenProductsWhenDeleteServiceRunExpectedItemDeleted() {
		List<Product> allProducts = productRepository.findAll();
		assertThat(allProducts, Matchers.hasSize(3));
		assertTrue(productRepository.existsById("2"));
		
		deleteProductService.process("2");
		
		allProducts = productRepository.findAll();
		assertThat(allProducts, Matchers.hasSize(2));
		Assert.assertFalse(productRepository.existsById("2"));
	}
	
	@Test
	public void givenProductsWhenDeleteServiceRunExpectedReturnOk() {
		assertThat(deleteProductService.process("1"), Matchers.equalTo(HttpStatus.OK));
	}
	
	@Test
	public void givenProductsWhenDeleteServiceRunWithNoexistentIdExpectedReturnError() {
		assertThat(deleteProductService.process("100"), Matchers.equalTo(HttpStatus.NOT_FOUND));
	}	
}
