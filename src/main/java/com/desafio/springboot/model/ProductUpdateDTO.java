package com.desafio.springboot.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProductUpdateDTO {
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("description")
	private String description;
	
	@JsonProperty("price")
	private Double price;

	public ProductUpdateDTO() {
	}
	
	private ProductUpdateDTO(ProductUpdateDtoBuilder productUpdateDtoBuilder) {
		this.name = productUpdateDtoBuilder.name;
		this.description = productUpdateDtoBuilder.description;
		this.price = productUpdateDtoBuilder.price;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public Double getPrice() {
		return price;
	}
	
	public static class ProductUpdateDtoBuilder {
		
		private String name;
		private String description;
		private Double price;
		
		public ProductUpdateDtoBuilder() {
		}
		
		public ProductUpdateDtoBuilder setName(String name) {
			this.name = name;
			return this;
		}
		
		public ProductUpdateDtoBuilder setDescription(String description) {
			this.description = description;
			return this;
		}
		
		public ProductUpdateDtoBuilder setPrice(Double price) {
			this.price = price;
			return this;
		}
		
		public ProductUpdateDTO build () {
			return new ProductUpdateDTO(this);
		}
	}
}
