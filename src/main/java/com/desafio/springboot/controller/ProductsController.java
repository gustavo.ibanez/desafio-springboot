package com.desafio.springboot.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.desafio.springboot.dto.ErrorMenssageEnum;
import com.desafio.springboot.dto.ResponseErrorDTO;
import com.desafio.springboot.model.Product;
import com.desafio.springboot.model.ProductUpdateDTO;
import com.desafio.springboot.service.DeleteProductService;
import com.desafio.springboot.service.NewProductService;
import com.desafio.springboot.service.SearchProductService;
import com.desafio.springboot.service.UpdateProductService;

@RestController
@RequestMapping("/api/products")
public class ProductsController {

	@Autowired
	private NewProductService newProductsService;

	@Autowired
	private UpdateProductService updateProductsService;
	
	@Autowired
	private DeleteProductService deleteProductsService;
	
	@Autowired
	private SearchProductService searchProductService;
	
	@GetMapping
	public List<Product> findAllProducts( ) {
		return searchProductService.findAllProducts();
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<Product> findProduto(@PathVariable(value = "id") String idProduto) {
		final Optional<Product> product = searchProductService.findById(idProduto);
		if (product.isPresent()) {
			return new ResponseEntity<>(product.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
	}
	
	@PostMapping
	public ResponseEntity<?> newProduct(@RequestBody Product products) {
		try {
			return new ResponseEntity<Product>(newProductsService.process(products), HttpStatus.CREATED);
		} catch (Exception e) {
			return buildErrorReturn(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	@PutMapping(value="/{id}")
	public ResponseEntity<?> updateProduct(@RequestBody ProductUpdateDTO products,
													   @PathVariable(value = "id") String idProduto) {
		try {
			final Product product = updateProductsService.process(idProduto, products);
			if (product == null) {
				return buildErrorReturn(HttpStatus.NOT_FOUND, ErrorMenssageEnum.PRODUCT_NOT_FOUND.getMessage());
			} else {
				return new ResponseEntity<Product>(product, HttpStatus.OK);
			}
		} catch (Exception e) {
			return buildErrorReturn(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Serializable> deleteProduct(@PathVariable(value = "id") String idProduto) {
		return new ResponseEntity<>(deleteProductsService.process(idProduto));
	}
	
	@GetMapping(value="/search")
	public List<Product> searcProduct(@RequestParam(value = "name_description", required=false) String valueQ,
													@RequestParam(value = "min_price", required=false) Double valueMin,
													@RequestParam(value = "max_price", required=false) Double valueMax) {
		return searchProductService.process(valueQ, valueMin, valueMax);
	}
	
	private ResponseEntity<ResponseErrorDTO> buildErrorReturn(HttpStatus httpStatus, String message) {
		final ResponseErrorDTO responseErro = new ResponseErrorDTO(httpStatus.value(), message);
		return new ResponseEntity<ResponseErrorDTO>(responseErro, httpStatus);
	}
	
}
