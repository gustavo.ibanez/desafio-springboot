package com.desafio.springboot.util;

import com.desafio.springboot.dto.ErrorMenssageEnum;

public class InvalidValueProductPriceException extends Exception{

	private static final long serialVersionUID = 1L;

	public InvalidValueProductPriceException() {
		super(ErrorMenssageEnum.INVALID_VALUE_PRODUCT_PRICE.getMessage());
	}
}
