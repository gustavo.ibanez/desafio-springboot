package com.desafio.springboot.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.desafio.springboot.DeleteProductServiceTest;
import com.desafio.springboot.NewProductServerTest;
import com.desafio.springboot.ProductsControllerTest;
import com.desafio.springboot.SearchProductServiceTest;
import com.desafio.springboot.UpdateProductServiceTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	DeleteProductServiceTest.class,
	NewProductServerTest.class,
	ProductsControllerTest.class,
	SearchProductServiceTest.class,
	UpdateProductServiceTest.class
})

public class SuiteTests {
}
