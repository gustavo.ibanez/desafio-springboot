package com.desafio.springboot.util;

import com.desafio.springboot.dto.ErrorMenssageEnum;

public class InvalidProductDescriptionException extends Exception{

	private static final long serialVersionUID = 1L;

	public InvalidProductDescriptionException() {
		super(ErrorMenssageEnum.INVALID_PRODUCT_DESCRIPTION.getMessage());
	}
}
