package com.desafio.springboot.check;

import static org.apache.commons.lang3.StringUtils.isBlank;

import org.springframework.stereotype.Service;

import com.desafio.springboot.model.Product;
import com.desafio.springboot.util.InvalidProductDescriptionException;
import com.desafio.springboot.util.InvalidProductNameException;
import com.desafio.springboot.util.InvalidProductPriceException;
import com.desafio.springboot.util.InvalidValueProductPriceException;

@Service
public class ProductsCheck {
	
	public void check(Product product) throws Exception{
		
		if (isBlank(product.getName())) {
			throw new InvalidProductNameException();
		}
		
		if ( isBlank(product.getDescription())) {
			throw new InvalidProductDescriptionException();
		}

		if (product.getPrice() == null) {
			throw new InvalidProductPriceException();
		}
		
		if (product.getPrice() <= 0) {
			throw new InvalidValueProductPriceException();
		}
	}
}
